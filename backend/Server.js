const express = require("express");
const cors = require("cors");
const mysql = require("mysql");
const bcrypt = require("bcrypt");
const saltRounds = 10;

//! ASK ABOUT SECRETS "jsonstructure" && expiresIn :???

const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const session = require("express-session");

const jwt = require("jsonwebtoken");

const app = express();
app.use(
	cors({
		origin: ["http://localhost:3000"],
		methods: ["GET", "POST"],
		credentials: true,
	})
);
app.use(express.json());

app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(
	session({
		key: "userId",
		secret: "jsonStructureModify",
		resave: false,
		saveUninitialized: false,
		cookie: {
			expires: 60 * 60 * 24,
		},
	})
);

const db = mysql.createConnection({
	host: "localhost",
	user: "root",
	password: "",
	database: "convertX",
});

app.post("/register", (req, res) => {
	const username = req.body.username;
	const pwd = req.body.pwd;
	const email = req.body.email;

	bcrypt.hash(pwd, saltRounds, (err, hash) => {
		if (err) {
			console.log(err);
		}

		db.query(
			"INSERT INTO users (username,email,pwd)values(?,?,?)",
			[username, email, hash],
			(err, result) => {
				console.log(result);
			}
		);
	});
});

const virifyJWT = (req, res, next) => {
	const token = req.headers["x-access-token"];
	if (!token) {
		res.json({ auth: false, message: "needs a token ..." });
	} else {
		jwt.verify(token, "jsonstructure", (err, decoded) => {
			if (err) {
				res.json({ auth: false, message: "U failed to authenticate" });
			} else {
				req.userId = decoded.id;
				next();
			}
		});
	}
};

app.get("/isUserAuth", virifyJWT, (req, res) => {
	res.send({ auth: true, message: "You are authenticated !" });
});

app.get("/login", (req, res) => {
	if (req.session.user) {
		res.send({ loggedIn: true, user: req.session.user });
	} else {
		res.send({ loggedIn: false });
	}
});

app.post("/login", (req, res) => {
	const email = req.body.email;
	const pwd = req.body.pwd;

	db.query("SELECT * FROM users WHERE email = ?;", email, (err, result) => {
		if (err) {
			res.send({ err: err });
		}

		if (result.length > 0) {
			bcrypt.compare(pwd, result[0].pwd, (error, response) => {
				if (response) {
					const id = result[0].id;
					const token = jwt.sign({ id }, "jsonstructure", { expiresIn: 86400 });

					req.session.user = result;

					res.json({ auth: true, token: token, result: result });
				} else {
					res.json({ auth: false, message: "unvalide password" });
				}
			});
		} else {
			res.json({ auth: false, message: "unvalide email" });
		}
	});
});

app.listen("5000", () => {
	console.log("Server Running on port 5000");
});
