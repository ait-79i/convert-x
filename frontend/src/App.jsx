import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";
import { useEffect, useState } from "react";
import ConvertX from "./Components/ConvertX";
import Login from "./Components/Login/Login";
import RequireAuth from "./Components/Common/RequireAuth";
import APINetwork from "./Components/APINetwork";
import ModifyJsonStructureComp from "./Components/ModifyJsonStructureComp";
import { useAuth } from "./Components/Common/CommanFunctions";
import Home from "./Components/home/Home";
import About from "./Components/Common/About"; import Contact from "./Components/Common/Contact";
import Header from "./Components/header/Header";
import "./App.css";
import GettingStarted from "./Components/home/GettingStarted";
import ModifyJSON from "./Components/home/ModifyJSON";
import APIRequests from "./Components/home/APIRequests";
import ConvertExcel from "./Components/home/ConvertExcel";


function App() {
  const [logged, setlogged] = useState(false)
  const [bodyRequestData, setBodyRequestData] = useState({});

  useEffect(() => {
    setlogged(useAuth())
  })



  return (
    <div className="container">

      <Router>
        <Header />
        <Routes>
          {/* Public routes */}
          <Route
            path="/login"
            element={logged === false ? <Login choice={true} /> : <Navigate to="/" />}
          />
          <Route
            path="/register"
            element={logged === false ? <Login choice={false} /> : <Navigate to="/" />}
          />
          <Route path="/" element={<Home />} >
            <Route path="/" element={<GettingStarted />} />
            <Route path="getting-started" element={<GettingStarted />} />
            <Route path="Convert-excel" element={<ConvertExcel />} />
          <Route path="modify-json-structure" element={<ModifyJSON />} />
            <Route path="api-requests" element={<APIRequests />} />

          </Route>
          <Route path="/contact" element={<Contact withImg={true} />} />
          <Route path="/about" element={<About />} />

          {/* Protected routes */}
          <Route element={<RequireAuth />}>
            <Route
              path="/excel-to-json"
              element={<ConvertX setBodyRequestData={setBodyRequestData} />}
            />
            <Route
              path="/json-structure"
              element={
                <ModifyJsonStructureComp setBodyRequestData={setBodyRequestData} />
              }
            />
            <Route
              path="/api"
              element={<APINetwork bodyRequestData={bodyRequestData} />}
            />
          </Route>
          {/* Catsh all  */}
          <Route path="*" element={<h1> Not Found</h1>} />
        </Routes>
      </Router>

    </div>

  )
}

export default App