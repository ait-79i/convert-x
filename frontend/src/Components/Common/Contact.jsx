import { useForm } from "react-hook-form"

const Contact = ({ withImg }) => {

  const { register, handleSubmit, formState: { errors } } = useForm({ mode: "all" })

  const onSubmit = data => {
    console.log(data)

  }
  return (
    <div className="container row">
      <div className="col">

        {/* <!-- Default form contact --> */}
        <form className="text-center border border-light p-4" action="#!"

          onSubmit={handleSubmit(onSubmit)}
        >

          {/* <!-- Name --> */}
          <input {...register("name", {
            required: 'name is required',
            minLength: { value: 3, message: "username must be at least 3 characters long" },
            maxLength: { value: 30, message: "username must be almost 30 characters long" }
          }
          )}
            type="text"
            className="form-control mb-4"
            placeholder="Name"
          />
          <small className="text-danger">{errors.name?.message}</small>

          {/* <!-- Email --> */}
          <input {...register("email", {
            required: "Email is required",
            pattern: {
              value: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
              message: "email must be valid"
            }
          }
          )}
            className="form-control mb-4"
            placeholder="E-mail"
          />
          <p className="text-danger">{errors.email?.message}</p>
          {/* <!-- Subject --> */}

          <label>Subject</label>
          <select className="form-select mb-4"
            {...register("subject", {
              required: "Subject is required"
            })}
          >
            <option value="" disabled>Choose option</option>
            <option value="Feedback" >Feedback</option>
            <option value="Report a bug">Report a bug</option>
            <option value="Feature request">Feature request</option>
          </select>
          <small className="text-danger">{errors.subject?.message}</small>
          {/* <!-- Message --> */}
          <div className="form-group">
            <textarea {...register("message", {
              required: "Message is required",
              minLength: { value: 30, message: "Message must be at least 30 characters long" }
            })} className="form-control rounded"
              id="exampleFormControlTextarea2" rows="3" placeholder="Message"
            ></textarea>
            <small className="text-danger">{errors.message?.message}</small>
          </div>

          {/* <!-- Send button --> */}
          <button className="btn btn-info btn-block mt-4" type="submit"
          >Send</button>

        </form>
        {/* <!-- Default form contact --> */}
      </div>
      {
        withImg && <div className="col ">
          <div className="border border-light rounded">

            <img src="images/contact-us.jpg" alt="" />

          </div>
        </div>
      }

    </div>
  )
}

export default Contact