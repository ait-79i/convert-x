import React, { useState } from 'react'

const CopyToClipboard = ({ data }) => {
  const [copy, setCopy] = useState(true)
  return (
    <div
      className='json-copy'
    >
      <div className='json-hint'> Each element of your {data.length} elements looks like : </div>
      {copy ?
        <button className='btn-copy'
          onClick={() => {
            navigator.clipboard.writeText(JSON.stringify(data))
            setCopy(false)
            setTimeout(() => {
              setCopy(true)
            }, 3000);
          }}
          title='Copy all data to clipboard'
        >
          <span>
            <ion-icon name="clipboard-outline"></ion-icon>
          </span>
          
          Copy
        </button>

        :
        <button className='btn-copy'>
          <span>
            <ion-icon name="checkmark-sharp"></ion-icon>
          </span>
          Copied
        </button>
      }
    </div>
  )
}

export default CopyToClipboard