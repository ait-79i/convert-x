import { useEffect, useState } from "react";
import { useJwt } from "react-jwt";
import { Navigate, Outlet, useLocation, useNavigate } from "react-router-dom"
import axios from "axios";
import { useAuth } from "./CommanFunctions";


const RequireAuth = () => {
  const [logged, setlogged] = useState(true)
  useEffect(() => {
    validateToken()
    const logge = useAuth();
    setlogged(logge)
  }, []);
  const navigate = useNavigate()
  const { isExpired } = useJwt(localStorage.getItem("token"));

  const validateToken = () => {
    const token = localStorage.getItem("token");
    axios
      .get("http://localhost:5000/isUserAuth", {
        headers: { "x-access-token": token },
      }).then((response) => {
        if (response.data?.auth !== true || isExpired) {
          localStorage.clear()
          navigate('/login')
        }
      })
  };

  const location = useLocation();


  return (
    <>
      {logged ?
        <Outlet />
        :
        <Navigate to='/login' state={{ from: location }} replace />
      }
    </>
  )
}

export default RequireAuth

