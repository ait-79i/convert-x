import React from 'react'
import { Link } from 'react-router-dom'

const Navbar = () => {

  const logOut = () => {
    localStorage.clear()
  }

  return (
    <header>
      {/* <img src="" alt="" /> */}
      <Link className="link logo" to='/'>Convert<span className='X'>X</span></Link>
      <nav className="nav__links">
        <li><Link className="link" to='/excel-to-json'>Excel to json</Link></li>
        <li><Link className="link" to='/json-structure'>json structure</Link></li>
        <li><Link className="link" to='/test-api'>Test API</Link></li>
        <li><Link className="link" to='/contact'>Contact</Link></li>
        <li><Link className="link" to='/about'>about</Link></li>
      </nav>
      <Link className="link" to='/'><button className='header-btn' onClick={logOut}>log out</button></Link>
    </header>

  )
}

export default Navbar