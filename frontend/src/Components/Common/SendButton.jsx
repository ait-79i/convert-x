import React from 'react'
import { Link } from 'react-router-dom'


const SendButton = ({ data, setBodyRequestData }) => {
  return (
    <Link to='/api' 
      onClick={() => setBodyRequestData(data)}
    >
      <button>Send data</button>
    </Link>
  )
}

export default SendButton