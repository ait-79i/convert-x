const About = () => {
  return (
    <div className="container" style={{ marginTop: '30px' }}>


      <h1 style={{ textAlign: "center" }}>
        Welcome to Convert<span className='X'>X</span>.com!
      </h1>
      <div style={{ margin: '0 70px' }} >
        <div>
          Our platform offers a simple and effective solution for converting Excel files to JSON format and managing the resulting JSON data. Whether you're a developer, data analyst, or business owner, our tools and services can help you streamline your workflows and achieve your goals.
        </div>

        <div>
          How does it work? Simply upload your Excel file to our platform, and our advanced algorithms will automatically convert it to JSON format. You can then use our intuitive interface to manage and manipulate the resulting JSON data, making it easy to extract the insights and information you need.
        </div>
        <div>

          But our services don't stop there. We also offer a range of tools and resources to help you handle complex Excel structures and optimize your data management processes. From custom data mapping to advanced filtering and sorting options, our platform is designed to help you work smarter, not harder.
        </div>
        <div>
          So why choose ExcelToJson.com? Our team of experts has years of experience in data management and conversion, and we're committed to providing top-quality services and support to our clients. Whether you need help with a one-time conversion project or ongoing data management, we're here to help.
        </div>

        <div>
          Thank you for choosing ExcelToJson.com! We look forward to helping you achieve your data management goals.
        </div>
      </div>

    </div>
  )
}

export default About