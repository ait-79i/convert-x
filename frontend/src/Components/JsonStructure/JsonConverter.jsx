import React, { useState } from 'react';

const JsonConverter = () => {
  const [formattedJson, setFormattedJson] = useState({});

  const convertJson = (jsonObj, format) => {
    const newJson = {};
    for (let key in format) {
      if (typeof format[key] === 'object') {
        newJson[key] = convertJson(jsonObj[key] || {}, format[key]);
      } else {
        newJson[key] = jsonObj[key] || format[key];
      }
    }
    return newJson;
  };

  const handleConvert = () => {
    setFormattedJson(convertJson({
      "First Name": "ikhaani",
      "Last Name": "ikhan",
      "Gender": "Male",
      "Age": 23,
      "Email": "f.clark@randatmail.ikhan",
      "Phone": "0404040",
      "Education": "Primary",
      "Occupation": "Ikhan",
      "Experience (Years)": 4,
      "Salary": 1318,
      "Marital Status": "Married",
      "Number of Children": 0
    }, {
      "Occupation": "Chef",
      "Marital Status": "Married",
      "Experience (Years)": 0,
      "Number of Children": 5,
      "c": {
        "Custom column 1": 4,
        "Custom column 2": 4,
        "b": {
          "nmm": {
            "Salary": 3010,
            "Email": "r.carter@randatmail.com",
            "Age": 18
          },
          "A": {
            "First Name": "Rosie",
            "Last Name": "Carter",
            "Gender": "Female"
          }
        }
      },
      "fnfnfnfn": {
        "Phone": "650-5815-37",
        "Education": "Bachelor"
      }
    }));
  };

  return (
    <div>
      <button onClick={handleConvert}>Convert JSON</button>
      <pre>{JSON.stringify(formattedJson, null, 2)}</pre>
    </div>
  );
};

export default JsonConverter;
