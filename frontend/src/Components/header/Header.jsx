import { useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom'
import { useAuth } from "../Common/CommanFunctions";
const Header = () => {
  const navigate = useNavigate()
  const [logged, setlogged] = useState(false)

  useEffect(() => {
    setlogged(useAuth())
  })

  const logOut = () => {
    localStorage.clear()
    navigate('/excel-to-json')
  }
  const login = () => {

    navigate('/login')
  }
  const register = () => {

    navigate('/register')

  }
  return (

    <header className='container'>
      <Link className="link logo" to='/'>Convert<span className='X'>X</span></Link>
      <nav className="nav__links">
        <li>
          <Link className="link" to='/excel-to-json'>Excel to json</Link>
        </li>
        <li>
          <Link className="link" to='/json-structure'>json structure</Link>
        </li>
        <li>
          <Link className="link" to='/api'>API</Link>
        </li>
        <li>
          <Link className="link" to='/contact'>Contact</Link>
        </li>
        <li>
          <Link className="link" to='/about'>about</Link>
        </li>
      </nav>
      {
        logged
          ?
          <button className='header-btn' onClick={logOut}>log out</button>
          :
          <>
            <button className='header-btn' onClick={login}>Login</button>
            <button className='header-btn' onClick={register}>register</button>
          </>
      }

    </header>
  )
}

export default Header