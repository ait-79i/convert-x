import { useEffect, useState } from 'react'
import CopyToClipboard from './Common/CopyToClipboard'
import DownloadJson from './Common/DownloadJson'
import DropJsonFile from './Drag&Drop/DropJsonFile'
import JsonStructureFormatt from './JsonStructure/JsonStructureFormatt'
import DisplayJson from './displayJsonSyntax/DisplayJson'
import SendButton from './Common/SendButton'

const ModifyJsonStructureComp = ({ setBodyRequestData }) => {

  const [data, setData] = useState([])
  const firstJsonColumns = Object.keys(data[0] === undefined ? [] : data[0])

  const [culomns, setculomns] = useState([])
  useEffect(() => {
    setculomns(Object.keys(data[0] === undefined ? [] : data[0]))
  }, [data])

  return (
    <div>
      <DropJsonFile setData={setData} />

      {data.length !== 0 &&
        <div className='workspace'>
          <div className='wrapper'>
            <CopyToClipboard data={data} />
            <DisplayJson data={data} />
          </div>
          <div className='wrapper'>
            <JsonStructureFormatt
              culomns={culomns}
              setJsonFile={setData}
              jsonFile={data}
              xlsxculomns={firstJsonColumns}
            />
            <div className='json-btns'>

              <DownloadJson data={data} />

              <SendButton data={data} setBodyRequestData={setBodyRequestData} />
            </div>
            
          </div>
        </div>

      }

    </div >
  )
}

export default ModifyJsonStructureComp