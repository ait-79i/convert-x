import { useEffect, useState, useRef } from "react";
import { useNavigate } from "react-router-dom";
import * as Components from './LoginStyleComponents';
import { useForm } from "react-hook-form"
import axios from "axios";


function Login({ choice }) {
  // just for sign in and sign up ghost
  const [signIn, setSignIn] = useState(false);

  useEffect(() => {
    setSignIn(choice)
  }, [choice])

  const navigate = useNavigate();

  const emailRef = useRef()
  const errRef = useRef()
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [logginerror, setLogginerror] = useState('')

  useEffect(() => {
    emailRef.current.focus()
  }, [])

  useEffect(() => {
    setLogginerror('')
  }, [email, password])


  const login = (e) => {
    e.preventDefault()

    if (email !== '' && password !== '') {
      axios.post('http://localhost:5000/login', JSON.stringify({ email: email, pwd: password }), {
        headers: { 'Content-Type': 'application/json' },
        withCredentials: true
      }).then((response) => {
        if (!response.data.auth) {
          setLogginerror(response.data.message)
        } else {
          navigate('/')
          const accessToken = response?.data?.token
          localStorage.setItem("token", accessToken)
          // navigate('/')
          setEmail('')
          setPassword('')

        }
        
      }).catch((err) => {
        console.log(err);
        if (!err?.response) {
          setLogginerror("No server Response")

        }
        // else if (err?.response.status === 400) {
        //   setLogginerror('Missing Email or Password')

        // } else if (err?.response.status === 401) {
        //   setLogginerror('Unauthorazied')

        // } else {
        //   setLogginerror('Login Failed')

        // }
        errRef.current.focus()
      })
      //! temperary untel i regenerate backend
    } else {

      if (password === '') setLogginerror("Password can't be empty")
      if (email === '') setLogginerror("Email can't be empty")
      if (email === '' && password === '') setLogginerror("Email and password can't be empty")
    }
  }

  const { register, handleSubmit, formState: { errors } } = useForm({ mode: "all" })

  const onSubmit = data => {
    axios.post('http://localhost:5000/register', JSON.stringify({
      username: data.username,
      pwd: data.password,
      email: data.email
    }), {
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true
    })
    navigate('/login')
  }

  return (
    <section style={{ marginTop: '50px' }} className="cc_flex">
      <div >
        <Components.Container >
          <Components.SignUpContainer signinIn={signIn}>
            <Components.Form

              onSubmit={handleSubmit(onSubmit)}
            >
              <Components.Title>Create Account</Components.Title>
              <Components.Input
                {...register("username", {
                  required: "Username is required",
                  minLength: { value: 3, message: "username must be at least 3 characters long" },
                  maxLength: { value: 30, message: "username must be almost 30 characters long" },
                }
                )}
                type='text'
                placeholder='User Name' />
              <small className="text-danger">{errors.username?.message}</small>

              <Components.Input
                {...register("email", {
                  required: "Email is required",
                  pattern: {
                    value: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                    message: "email must be valid"
                  }
                }
                )}
                placeholder='Email'
              />
              <small className="text-danger">{errors.email?.message}</small>
              <Components.Input
                {...register("password", {
                  required: "Password is required",
                  pattern: {
                    value: /^.*(?=.{8,})(?=.*[a-zA-Z])(?=.*\d)(?=.*[!#$%&? "]).*$/,
                    message: 'Password must contain at least 6 characters , One Uppercase, one Lowercase , One Number,one Special character'
                  }
                }
                )}
                type='password'
                placeholder='Password' />
              <small className="text-danger">{errors.password?.message}</small>
              <Components.Button >Sign Up</Components.Button>
            </Components.Form>
          </Components.SignUpContainer>


          <Components.SignInContainer signinIn={signIn}>
            <Components.Form onSubmit={(e) => login(e)}>
              <Components.Title>Sign in</Components.Title>
              <Components.Input
                type='text'
                id='mail'
                placeholder='Email'
                value={email}
                ref={emailRef}
                onChange={(e) => { setEmail(e.target.value) }}
              />


              <Components.Input
                id="pwd"
                placeholder='Password'
                value={password}
                onChange={(e) => { setPassword(e.target.value) }}
              />
              <small ref={errRef} aria-live="assertive" style={{ color: 'red' }}>{logginerror}</small>
              <Components.Button >Sigin In</Components.Button>
            </Components.Form>
          </Components.SignInContainer>


          {/* top */}


          <Components.OverlayContainer signinIn={signIn}>
            <Components.Overlay signinIn={signIn}>

              <Components.LeftOverlayPanel signinIn={signIn}>
                <Components.Title>Welcome!</Components.Title>
                <Components.Paragraph>
                  To keep connected with us please login with your personal info
                </Components.Paragraph>
                <Components.GhostButton
                  onClick={() => {
                    setSignIn(true)
                  }}>
                  Sign In
                </Components.GhostButton>
              </Components.LeftOverlayPanel>

              <Components.RightOverlayPanel signinIn={signIn}>
                <Components.Title>Hi, Dear!</Components.Title>
                <Components.Paragraph>
                  Enter Your personal details and start journey with us
                </Components.Paragraph>
                <Components.GhostButton onClick={() => {
                  setSignIn(false)
                }}>
                  Sigin Up
                </Components.GhostButton>
              </Components.RightOverlayPanel>

            </Components.Overlay>
          </Components.OverlayContainer>

        </Components.Container>
      </div>
    </section>
  )
}

export default Login;