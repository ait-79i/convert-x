import { Link, Outlet } from 'react-router-dom'


const Home = () => {
  return (
    <div className='home'>
      <section className='container'>
        <div className="container row m-3 mt-5 " >
          <div className="col-3 bg-light p-4 rounded-start">
            <ul className='nav-ul mt-5'>
              <li>
                <Link to='/getting-started' className='text-decoration-none' >getting started</Link>
              </li>
              <li>
                <Link to='/Convert-excel' className='text-decoration-none'>Convert excel</Link>
              </li>
              <li>
                <Link to='/modify-json-structure' className='text-decoration-none'>Modify Json Structure</Link>
              </li>
              <li>
                <Link to='/api-requests' className='text-decoration-none'>API Requests</Link>
              </li>
            </ul>

          </div>
          <div className="col-9 px-5 border border-light rounded-end">
            <Outlet />
          </div>
        </div>
      </section>
    </div>
  )
}

export default Home