import React from 'react'

const APIRequests = () => {
  return (
    <section className=" row">
      <h1 style={{ textAlign: 'center' }}>API Requests</h1>
      <div className="col-6">
        <div className='api-text'>
          you can now utilize our API request feature to send your generated data or to test other API request.

          To get started, simply  explore the API area. This will provide you with all the information you need to make a request and send your data to any destination you want. Whether you're a developer or just someone who wants to streamline their workflow, this feature is perfect for you.

          So don't hesitate to take advantage of this great tool . If you have any questions or need assistance, please don't hesitate to reach out to our support team. We're always here to help you get the most out of our platform.

          Thank you for choosing our website, and we look forward to seeing the innovative ways you use our API request feature to enhance your workflow.
        </div>
      </div>
      <div className="api-img-container">

        <img src="/images/api.svg" alt="api" className="col-6 api-img " />
      </div>



    </section>
  )
}

export default APIRequests