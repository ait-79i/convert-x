import { Link } from "react-router-dom"

const GettingStarted = () => {
  return (
    <section id='getting-started' >

      <h1 className='text-left get-start mb-3'>Getting Started</h1>
      <div className="text-justify">
        Convert Excel to JSON with Ease
        <br />
        Say Goodbye to Manual Conversion and Get Accurate Results in Minutes
        Streamline Your Data Management with Our Excel-to-JSON Converter <Link to="/" className="text-decoration-none">Convert<span className='X'>X</span></Link>
        <br />
        while also giving you the freedom to modify the JSON structure and make API requests on the fly.
        <br />
        Get started today and start enjoying the benefits of streamlined data management.
      </div>
      <h4>Binifits</h4>
      <div>
        <ul className="binifits">
          <li>
            You can Read your Excel file data
          </li>
          <li>
            You can easily convert <Link to='/Convert-excel' className="text-decoration-none">Excel to JSON</Link>
          </li>
          <li>
            You can modify JSON keys
          </li>
          <li>
            You can <Link to='/modify-json-structure' className="text-decoration-none">Change JSON structure</Link> when converting or Download an intire file and control it
          </li>
          <li>
            You can Download your converted data as a json file
          </li>
          <li>
            You can Copy your converted data to Clipboard
          </li>
          <li>
            You can Send your converted data via API Request
          </li>
          <li>
            You can Test and Do your <Link to='/api-requests' className="text-decoration-none">API Request </Link>
          </li>
        </ul>

      </div>
    </section>
  )
}

export default GettingStarted