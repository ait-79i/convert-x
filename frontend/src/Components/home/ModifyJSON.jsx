import React from 'react'
import { Link } from 'react-router-dom'

const ModifyJSON = () => {
  return (
    <section id='json-structure' >

      <h1 className='text-left get-start'>Modify Json Structure</h1>
      <div>Attention, JSON enthusiasts! 📣 Want to modify the structure of your JSON file with ease? Look no further! 🔍 Whether you're a seasoned developer or a coding newbie, our user-friendly tool makes restructuring your JSON a breeze. Simply select your file or drag it into our platform, and our intuitive interface will guide you through the process step-by-step. 💻
        <br />

        Don't waste your valuable time struggling with complex coding tools or manual edits. Our platform streamlines the process, so you can focus on what really matters: creating amazing applications and pushing the boundaries of what's possible. 🚀 So why wait? Start revamping your JSON structure today and take your development skills to the nextlevel.
      </div>
      <div className='mt-3'>
        <ol className=".nav-ul">
          <li>
            Locate the option to upload or drag and drop a JSON file <Link to='/json-structure'>here</Link>.
          </li>
          <li>
            Select the JSON file from your computer or drag and drop it into the designated area on the website.
          </li>
          <li>
            Once the JSON file is uploaded, locate the key that you want to check and wrap under a new key.
          </li>

          <li>
            Select the key by clicking on it.
          </li>

          <li>
            Enter a name for the new key.
          </li>

          <li>
            Click on the generate button the selected key and choose the option to wrap it under a new key.
          </li>

          <li>
            Once the new key is created, you can start making changes to the JSON structure by adding or removing keys as needed.
          </li>

          <li>
            After making the desired changes, changes are automaticly saved you can download the modified JSON file by clicking on the download button or copy  to copy file data to clipboard or send it by pressing send button.
          </li>
          That's it! Start making changes to the JSON structure as needed.
        </ol>
      </div>
    </section>
  )
}

export default ModifyJSON