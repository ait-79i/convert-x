import React, { useEffect, useState } from 'react';
import DropExcelFile from './Drag&Drop/DropExcelFile';
import DisplayTable from './tabaleData/DisplayTable';
import DisplayJson from './displayJsonSyntax/DisplayJson';
import JsonCols from './JsonStructure/JsonCols';
import DownloadJson from './Common/DownloadJson';
import JsonStructureFormatt from './JsonStructure/JsonStructureFormatt';
import CopyToClipboard from './Common/CopyToClipboard';
import SendButton from './Common/SendButton';
import { PreventReload } from './Common/CommanFunctions';
import JsonConverter from './JsonStructure/JsonConverter';

function ConvertX({ setBodyRequestData }) {

  const [data, setData] = useState([]);
  const xlsxculomns = Object.keys(data[0] === undefined ? [] : data[0]);
  const [jsonFile, setJsonFile] = useState([])
  const [culomns, setCulomns] = useState([])
  const [displayExcelTable, setdisplayExcelTable] = useState(true)

  useEffect(() => {
    setCulomns(Object.keys(jsonFile[0] === undefined ? [] : jsonFile[0]))
  }, [jsonFile])

  useEffect(() => {
    setCulomns(xlsxculomns)
    setJsonFile([...data])
  }, [data])

  PreventReload()
  const [preventUpdateTableCols, setpreventUpdateTableCols] = useState(false)

  const [withFormat, setwithFormat] = useState(false)

  return (

    <div>
      {/* ------------- Drag And Drop Zone ----------- */}
      <DropExcelFile setData={setData} />

      <div>
        {data.length > 0
          &&
          <div>

            <div className='workspace'>

              {/* //----------- display formatted json --------*/}
              <div className='wrapper' >
                <CopyToClipboard data={jsonFile} />
                <DisplayJson data={jsonFile} />
              </div>

              {/* //----------- Modify JSON structure area --------*/}

              <div className='wrapper'>
                {withFormat
                  ?
                  <JsonConverter />
                  :
                  <JsonStructureFormatt
                    culomns={culomns}
                    setJsonFile={setJsonFile}
                    jsonFile={jsonFile}
                    xlsxculomns={xlsxculomns}
                    JsonCols={JsonCols}
                    setpreventUpdateTableCols={setpreventUpdateTableCols}
                  />}


                <div className='json-btns'>
                  {/* //--------------Download json data ----------------- */}

                  <DownloadJson
                    data={jsonFile}
                  />

                  {/* //--------------Send new json format to api component ----------------- */}

                  <SendButton
                    data={jsonFile}
                    setBodyRequestData={setBodyRequestData}
                  />

                  <input type="button" value={displayExcelTable ? 'hide excel table' : 'display excel table'}
                    onClick={() => setdisplayExcelTable(!displayExcelTable)}
                  />
                  {/* 
                  <input type="button" value={withFormat ? 'manual format' : 'give eg format'}
                    onClick={() => setwithFormat(!withFormat)}
                  /> */}

                </div>
              </div>

            </div>
            {/* ------------ Excel table ---------- */}
            {
              displayExcelTable &&
              <DisplayTable
                preventUpdateTableCols={preventUpdateTableCols}
                data={data}
                culomns={xlsxculomns}
                setJsonFile={setJsonFile}
              />
            }

          </div>
        }
      </div >
    </div>
  );
}

export default ConvertX;